if wscript.arguments.length < 1 or wscript.arguments.length > 2 then
	wscript.echo "SYNOPSIS"
	wscript.echo "   disksinfo list [disknumber]"
	wscript.echo
	wscript.echo "DESCRIPTION"
	wscript.echo "   List information from disk drives"
	wscript.echo
	wscript.echo "EXIT CODES"
	wscript.echo "   123   If listing was successful"
	wscript.quit
end if

set wmi = getObject("winmgmts:\\.\root\cimv2")

if wscript.arguments.length = 1 then
	' list disks
	set disks = wmi.execQuery("select deviceid,index,model,partitions,pnpdeviceid,size from win32_diskdrive",,wbemFlagReturnWhenComplete or wbemFlagForwardOnly)
	
	if err.number <> 0 then
		wscript.echo "Error querying Win32_DiskDrive: (" & err.number & "): " & err.description
		wscript.quit
	end if
	
	for each disk in disks
		deviceID = replace(disk.deviceID, "\", "\\")
		set diskDetails = wmi.execQuery("select serialnumber,tag from win32_physicalmedia where tag='" & deviceID & "'",,wbemFlagReturnWhenComplete or wbemFlagForwardOnly)
		
		if err.number <> 0 then
			wscript.echo "Error querying Win32_PhysicalMedia: (" & err.number & "): " & err.description
			wscript.quit
		end if

		if diskDetails.count <> 1 then
			wscript.echo "No match found in Win32_PhysicalMedia(.Tag) for Win32_DiskDrive(.DeviceID)"
			wscript.quit
		end if
		
		' collection item access: unbelievable shit: http://social.technet.microsoft.com/Forums/nl/ITCG/thread/1d21cc55-e510-4849-b954-c5a141d95ec3
		for each diskDetail in diskDetails
			serialNumber = trim(diskDetail.serialNumber)
			exit for ' first match only, see comment above
		next
		
		wscript.echo "DISKNUM:" & disk.index & ",MODEL:" & disk.model & ",SERIAL:" & serialNumber & ",SIZE:" & disk.size & ",PARTITIONS:" & disk.partitions & ",PATH:" & disk.deviceID & ",DEVICEID:" & disk.pnpDeviceID

	next

else
	' list disk's partitions
	diskNum = wscript.arguments(1)
	set re = new regexp
	re.pattern = "^[0-9]+$"
	if not re.test(diskNum) then
		wscript.echo "Disk number must be numbers"
		wscript.quit
	end if
	
	set partitions = wmi.execQuery("select index,size from win32_diskpartition where diskindex=" & diskNum,,wbemFlagReturnWhenComplete or wbemFlagForwardOnly)
	
	if err.number <> 0 then
		wscript.echo "Error querying Win32_DiskPartition: (" & err.number & "): " & err.description
		wscript.quit
	end if
	
	for each partition in partitions
		wscript.echo "PARTITION:" & partition.index & ",SIZE:" & partition.size
	next
	
end if

wscript.quit 123
