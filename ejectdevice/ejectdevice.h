#pragma once

#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <Cfgmgr32.h>

PTSTR getSysErrMsg(DWORD errCode);

#define USAGE "\
SYNOPSIS\n\
   ejectdevice [deviceid]\n\
\n\
DESCRIPTION\n\
   Eject device by deviceid*\n\
    * SOURCES\n\
      - Gima's _disksinfo.vbs\n\
      - WMI: Win32_DiskDrive.PNPDeviceID\n\
      - Microsoft DevCon Utility\n\
      - Device Manager\n\
        .. -> Device Properties\n\
        ..   -> Details\n\
        ..     -> Device Instance Path\n\
    * EXAMPLES\n\
      \"IDE\\DISKHITACHI_ABCDEFGHI_______________________12345678\\0&12345678&0&0.0.1\"\n\
\n\
EXIT CODES\n\
   0      Eject was issued successfully\n\
   ????   Everything else is an error\n\
\n\
RETURNS\n\
   Nothing if everything went as expected\n\
   An error message, in case of an error\n"