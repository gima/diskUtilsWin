#include "ejectdevice.h"

int _tmain(int argc, _TCHAR* argv[]) {

	DEVINST devInst; // needs closing?
	DWORD ret;
	TCHAR errMsg[1024];

	if (argc != 2) {
		printf(USAGE);
		return 1;
	}

	// wprintf(L"Device Instance ID: %s\n", argv[1]);

	// using supplied device id, find some kind of handle needed by other CM_ functions
	ret = CM_Locate_DevNode((PDEVINST) &devInst, argv[1], CM_LOCATE_DEVNODE_NORMAL);
	if (ret != CR_SUCCESS) {
		PTSTR translatedMsg = getSysErrMsg(ret);
		wprintf(L"CM_LocateDevNode failed: %s", translatedMsg);
		delete translatedMsg;
		return 1;
	}

	// try to eject the device represented by the handle
	ret = CM_Request_Device_Eject(devInst, NULL, errMsg, sizeof(errMsg), 0);
	if (ret != CR_SUCCESS) {
		PTSTR translatedMsg = getSysErrMsg(ret);
		wprintf(L"CM_Request_Device_Eject failed: %s: %s", errMsg, translatedMsg);
		delete translatedMsg;
		return 1;
	}

	return 0;
}

PTSTR getSysErrMsg(DWORD errCode) {
	PTSTR formattedMsg;
	PTSTR outFormattedMsg = new TCHAR[1024];

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errCode, NULL, (PTSTR) &formattedMsg, 0, NULL );

	wsprintf(outFormattedMsg, L"(%d) %s", errCode, formattedMsg);

	LocalFree(formattedMsg);
	return outFormattedMsg;
}
