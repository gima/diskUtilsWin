### DESCRIPTION

Command line utilities to list disk details + partitions, eject device by id, set disks online/offline 

### SCREENSHOTS (yeah...)

![](https://raw.githubusercontent.com/gima/diskUtilsWin/master/screenshots/disksinfo.png)

![](https://raw.githubusercontent.com/gima/diskUtilsWin/master/screenshots/ejectdevice.png)

![](https://raw.githubusercontent.com/gima/diskUtilsWin/master/screenshots/disksetline.png)
