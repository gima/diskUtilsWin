if wscript.arguments.length <> 2 then
	wscript.echo "SYNOPSIS"
	wscript.echo "   disksetline [disknumber] [online/offline]"
	wscript.echo
	wscript.echo "DESCRIPTION"
	wscript.echo "   Command disk drive online/offline by disknumber"
	wscript.echo
	wscript.echo "EXIT CODES"
	wscript.echo "   123   If command was issued successfully"
	wscript.quit
end if

diskNum = wscript.arguments(0)
newStatus = wscript.arguments(1)

select case newStatus
	case "online"
	case "offline"
	case else
		wscript.echo "Command must be either 'online' or 'offline'"
		wscript.quit
end select

set shell = wscript.createObject("wscript.shell")

set process = shell.exec("cmd /c (echo select disk " & diskNum & " && echo " & newStatus & " disk) | diskpart")
all = process.stdout.readall

set re = new regexp
re.pattern = "DiskPart successfully " & newStatus & "d the selected disk."
if not re.test(all) then
	wscript.echo "Error:diskpart:" & vbCrLf & all
	wscript.quit
end if

wscript.quit 123
